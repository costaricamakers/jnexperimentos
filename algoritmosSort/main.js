/*
 * Este es código de dominio público escrito por Jose Nunez como una adaptación del código 
 * publicado por Nicolas M. Toscano en  https://medium.com/@ntoscano/bubble-sort-vs-quick-sort-6ff48280f745 
 * Pretende demostrar el funcionamiento de los algoritmos quicksort y bubblesort y su desempeño.
 */

var qsSteps = 0;
var quickSort = function(array){
  if (array.length === 0) return [];
  var left = [], right = [], pivot = array[0];
  for (var i = 1; i < array.length; i++) {
    qsSteps++;
    array[i] < pivot ? left.push(array[i]) : right.push(array[i]);
  }
  return quickSort(left).concat(pivot, quickSort(right));
};


var bsSteps = 0;
var bubbleSort = function (array) {
    var count = array.length - 1;
    for (var x = 0; x < array.length; x++) {
        var changes = 0;
        for (var m = 0; m < count; m++) {
            var val = array[m];
            if (val > array[m + 1]) {
                array[m] = array[m + 1];
                array[m + 1] = val;
                changes++;
                bsSteps++;
            }
        }
        count--;
        if (changes === 0) {
            return array;
        }
    }
    return array;
};

var myArray = [];
var elements = 2000;
for(var i=0; i<elements; i++){
    myArray.push(Math.floor((Math.random() * elements) + 1));
}

var qsTimeInit, qsTimeEnd;
console.log(myArray);
qsTimeInit = Date.now();
var sortedArray = quickSort(myArray);
console.log(sortedArray);
qsTimeEnd = Date.now();

var bsTimeInit, bsTimeEnd;
console.log(myArray);
bsTimeInit = Date.now();
var sortedArray = bubbleSort(myArray);
console.log(sortedArray);
bsTimeEnd = Date.now();

console.log("QuickSort: duración: " + (qsTimeEnd - qsTimeInit)  + "ms", "Pasos: " + qsSteps);
console.log("BubblSort: duración: " + (bsTimeEnd - bsTimeInit)  + "ms", "Pasos: " + bsSteps);